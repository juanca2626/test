<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {

        $user = User::firstWhere('user', $request->user);

        if (!$user || !Hash::check($request->password, $user->password)) {

            throw new HttpResponseException(response()->json(['errors' => __('auth.failed')], Response::HTTP_UNPROCESSABLE_ENTITY));
        }

        return new UserResource($user);

    }
}
